defmodule EmailerCommonClient.GenericPasswordlessTemplate do
  defstruct head: nil,
            button: nil

  @type t() :: %__MODULE__{
          head: EmailHead.t(),
          button: EmailButton.t()
        }
end

defmodule EmailerCommonClient.EmailButton do
  defstruct url: ""

  @type t() :: %__MODULE__{
          url: String.t()
        }
end

defmodule EmailerCommonClient.EmailHead do
  defstruct host: "",
            svg: "",
            alt: "",
            title: ""

  @type t() :: %__MODULE__{
          host: String.t(),
          svg: String.t(),
          alt: String.t(),
          title: String.t()
        }
end

defmodule EmailerCommonClient.Email do
  defstruct subject: "",
            from: "",
            to: ""

  @type t() :: %__MODULE__{
          subject: String.t(),
          from: String.t(),
          to: String.t()
        }
end
