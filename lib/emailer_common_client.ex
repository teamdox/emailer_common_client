defmodule EmailerCommonClient do
  use Application
  require Logger

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = []

    opts = [strategy: :one_for_one, name: __MODULE__.Supervisor]
    s = Supervisor.start_link(children, opts)

    Logger.info("EmailerCommonClient app started ...")
    s
  end

  def ping(), do: :pong

  def call(c), do: "emailserver" |> :pg2.get_closest_pid() |> GenServer.call(c)
end
